Name:           lceda-pro
Version:        2.2.27.1
Release:        1%{?dist}
Summary:        专业、强大的国产 PCB 设计工具，永久免费

License:        Business Software
URL:            https://pro.lceda.cn/
Source0:        %{name}-%{version}.x86_64.tar.gz

BuildArch:      x86_64

%description


%prep
%setup -q


%build


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p %{buildroot}/opt/%{name}/
cp -rp %{_builddir}/%{name}-%{version}/* %{buildroot}/opt/%{name}/


%files
/opt/%{name}/


%pre
rm -rf /opt/%{name}/


%post
chmod -R 755 /opt/%{name}/
mkdir -p /usr/share/applications/
cp /opt/%{name}/lceda-pro.dkt /usr/share/applications/%{name}.desktop


%postun
rm -f /usr/share/applications/%{name}.desktop
